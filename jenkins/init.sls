{% from "jenkins/map.jinja" import jenkins with context %}

jenkins_group:
  group.present:
    - name: {{ jenkins.group }}
    - system: True

jenkins_user:
  file.directory:
    - name: {{ jenkins.home }}
    # Required to not allow the Jenkins user to delete files,
    # otherwise it is not possible to prevent it from altering
    # its config by recreating the file with himself as owner
    - user: root
    - group: {{ jenkins.group }}
    - mode: '1775'
    - require:
      - user: jenkins_user
      - group: jenkins_group
  user.present:
    - name: {{ jenkins.user }}
    - groups:
      - {{ jenkins.group }}
    - system: True
    - home: {{ jenkins.home }}
    - shell: /bin/bash
    - require:
      - group: jenkins_group

{% if jenkins.proxy is defined %}
jenkins_proxyconfig:
  file.managed:
    - name: {{ jenkins.home }}/proxy.xml
    - source: salt://jenkins/files/proxy.xml.j2
    - template: jinja
{% endif %}

{% if jenkins.config_xml is defined %}
jenkins_config_xml:
  file.managed:
    - name: {{ jenkins.home }}/config.xml
    - contents: |
        {{ jenkins.config_xml|indent(8) }}
    # Do not allow the jenkins to alter its config file
    # if it is managed by Salt
    - user: root
    - group: root
    - mode: '0644'
{% endif %}

{% if jenkins.known_hosts|length >0 %}
jenkins_ssh_dir:
  file.directory:
    - name: {{ jenkins.home }}/.ssh
    - user: {{ jenkins.user }}
    - group: {{ jenkins.group }}
    - mode: '0700'

## The file.line state is pretty stupid, it can
## * not create a file if it doesnt exist
## * only ensure a line exists exactly once if there is a marker already present in the file
## ==> bash ftw
{% for host_key in jenkins.known_hosts %}
jenkins_known_hosts_key_{{ loop.index }}:
  cmd.run:
    - name: >
        if ! [ -e {{ jenkins.home }}/.ssh/known_hosts ]; then touch {{ jenkins.home }}/.ssh/known_hosts; fi;
        if ! grep -q '{{ host_key }}' {{ jenkins.home }}/.ssh/known_hosts; then
          echo '{{ host_key }}' >> {{ jenkins.home }}/.ssh/known_hosts;
        fi;
        chown {{ jenkins.user }}:{{ jenkins.group }} {{ jenkins.home }}/.ssh/known_hosts;
        chmod 0600 {{ jenkins.home }}/.ssh/known_hosts
{% endfor %}
{% endif %}

{% if jenkins.custom_configs|default([])|length > 0 %}
{% for config in jenkins.custom_configs %}
jenkins_customconfig_{{ config.name }}:
  file.managed:
    - name: {{ jenkins.home }}/{{ config.name }}{% if not config.name.endswith('.xml') %}.xml{% endif %}
    - contents: '{{ config.content }}'
{% endfor %}
{% endif %}

jenkins_unitfile:
  file.managed:
    - name: /etc/systemd/system/jenkins.service
    - source: salt://jenkins/files/jenkins.service.j2
    - template: jinja

jenkins_binary:
  file.managed:
    - name: {{ jenkins.binary_destination }}-{{ jenkins.version }}
    - source: {{ jenkins.download_baseurl }}/{{ jenkins.version }}/jenkins.war
    - source_hash: {{ jenkins.checksums[jenkins.version] }}

jenkins_link:
  file.symlink:
    - name: {{ jenkins.binary_destination }}
    - target: {{ jenkins.binary_destination }}-{{ jenkins.version }}

jenkins:
  service.running:
    - enable: True
    - watch:
      - file: jenkins_link
      - file: jenkins_unitfile
{% if jenkins.proxy is defined %}
      - file: jenkins_proxyconfig
{% endif %}
{% if jenkins.custom_configs|default([])|length > 0 %}{% for config in jenkins.custom_configs %}
      - file: jenkins_customconfig_{{ config.name }}
{% endfor %}
{% endif %}
{% if jenkins.config_xml is defined %}
      - file: jenkins_config_xml
{% endif %}
